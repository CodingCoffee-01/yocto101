#  Busybox for initrd 

ref from : https://blog.csdn.net/wangyijieonline/article/details/103181899
           https://emreboy.wordpress.com/2012/12/20/building-a-root-file-system-using-busybox/

git clone https://git.busybox.net/busybox -b 1_xx_stable

cd busybox 

make defconfig

###  defconfig setting 

-> Settings

　--- Support --long-options

　　[\*] Don't use /usr		(CONFIG_INSTALL_NO_USR)


　--- Build Options

　　[\*] Build static binary (no shared libs)  	(CONFIG_STATIC)

　--- Installation Options ("make install" behavior)

　　What kind of applet links to install (<choice> [=y])

　　　(X) as soft-links	(CONFIG_INSTALL_APPLET_SYMLINKS)

　--- Library Tuning

　　[\*]   Query cursor position from terminal	(CONFIG_FEATURE_EDITING_ASK_TERMINAL)

###  --------------------
###  make initrd steps 
make (ARCH=aarch64 CROSS_COMPILE=aarch64-linux-gnu-) all

mkdir -p ./installed

make CONFIG_PREFIX=./installed install 

mkdir ../rootfs		
cd ../rootfs				
mkdir dev etc lib usr var proc tmp home root mnt sys

cd ../busybox/	

cp -ra installed/* ../rootfs

cp -ra examples/bootfloppy/etc/*  ../rootfs/etc	

cd ../rootfs/etc		

cp /etc/passwd .	

cp /etc/group .		

cp /etc/shadow .	
###  --------------------
###  modify related files

#### nano inittab  , Change to  no need login and start shell 

::sysinit:/etc/init.d/rcS

console::askfirst:-/bin/sh

::ctrlaltdel:/bin/umount -a -r

#### nano profile , add the following part 

PATH=/bin:/sbin:/usr/bin:/usr/sbin
export LD_LIBRARY_PATH=/lib:/usr/lib
/bin/hostname osee
USER="`id -un`"
LOGNAME=$USER
HOSTNAME='/bin/hostname'
PS1='[\u@\h \W]# '

#### nano init.d/rcS

/bin/mount -n -t ramfs ramfs /var
/bin/mount -n -t ramfs ramfs /tmp
/bin/mount -n -t sysfs none /sys
/bin/mount -n -t ramfs none /dev
/bin/mkdir -p  /var/tmp
/bin/mkdir -p  /var/modules
/bin/mkdir -p  /var/run
/bin/mkdir -p  /var/log
/bin/mkdir -p /dev/pts						
/bin/mkdir -p /dev/shm						
#echo /sbin/mdev > /proc/sys/kernel/hotplug 
/sbin/mdev -s         						
/bin/mount -a

####   network setting
/sbin/ifconfig lo 127.0.0.1 netmask 255.0.0.0
/sbin/ifconfig eth0 192.168.1.70
/sbin/ifconfig eth0 netmask 255.255.255.0
/sbin/route add default gw 192.168.1.1 eth0
/sbin/ifconfig eth1 192.168.1.71 netmask 255.255.255.0
/sbin/route add default gw 192.168.1.1 eth1

#### nano fstab , add 

none   /dev/pts    devpts   mode=0622      0 0

tmpfs  /dev/shm    tmpfs    defaults       0 0

####  nano shadow , group , passwd , just keep first line 

####  cp -ra /usr/lib/x86_64-linux-gnu/* rootfs/lib/

#### make modules && make modules_install INSTALL_MOD_PATH=/rootfs/lib/modules   (need to double check, skip)

####  package rootfs 

cd rootfs

find . | cpio -H newc -ov --owner root:root > ../initramfs.cpio

cd ..

gzip initramfs.cpio